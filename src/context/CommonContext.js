import React, { createContext, useContext, useReducer } from "react";

const StorageData = createContext(null);
const TellerBank = createContext(null);
const CommonContext = ({ Children }) => {
  const initialTask = [
    { title: "hello red", color: "red" },
    { title: "hello blue", color: "blue" },
  ];

  const tasksReducer = (tasks, action) => {
    switch (action.type) {
      case "panggil": {
        return [
          ...tasks,
          {
            id: action.id,
            text: action.text,
            done: false,
          },
        ];
      }

      default:
        throw Error("Unknown action: " + action.type);
    }
  };

  const [tasks, dispatch] = useReducer(tasksReducer, initialTask);

  return (
    <div>
      <StorageData.Provider value={tasks}>
        <TellerBank.Provider value={dispatch}>
          {Children}
        </TellerBank.Provider>
      </StorageData.Provider>
    </div>
  );
};

export default CommonContext;

export const useGetData = () => {
    return useContext(StorageData);
}

export const useFungsiDispatch = () => {
    return useContext(TellerBank);
}