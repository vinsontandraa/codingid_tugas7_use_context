import { Button } from "@mui/material";
import React from "react";
import { useHistory } from "react-router-dom";

const Navbar = () => {
  let history = useHistory();
  return (
    <div>
      <li>
        <span onClick={()=>{history.push("/")}}>Home</span>
      </li>
      <li>
        <Button onClick={()=>{history.push("/login")}}>
          Login
        </Button>
      </li>
    </div>
  );
};

export default Navbar;
