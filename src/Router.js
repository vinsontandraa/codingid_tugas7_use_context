import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Children from './page/Children'
import Home from './page/Home'
import Login from './page/Login'

const Router = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path={'/'}><Home/></Route>
          <Route exact path={'/login'}><Login/></Route>
          <Route exact path={'/children'}><Children/></Route>
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default Router
